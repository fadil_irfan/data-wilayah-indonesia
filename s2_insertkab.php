<?php 

$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '';
$dbname = 'wilayah';
$dbdsn = "mysql:dbname={$dbname};host={$dbhost}";
try {
  $db = new PDO($dbdsn, $dbuser, $dbpass);
} catch (PDOException $e) {
  echo 'Connection failed: '.$e->getMessage();
}

// INSERT kode_prov, kode_kab, nama_kab
$sqlstr = "INSERT INTO wilayah_kabupaten(kode_prov, kode_kab, nama_kab) ";
$sqlstr = $sqlstr . "SELECT left(kode, 2), kode, nama ";
$sqlstr = $sqlstr . "FROM wilayah where char_length(kode)=5 ORDER BY kode";

$query = $db->prepare($sqlstr);
$query->execute();
$query = null;

// UPDATE nama_prov
$sqlstr = "SELECT kode, nama FROM wilayah WHERE CHAR_LENGTH(kode)=2";
$query=$db->prepare($sqlstr);
$query->execute();

while ($data=$query->fetchObject()) {
    echo $data->kode.' '.$data->nama.PHP_EOL;
    $sqlstr_upd = "UPDATE wilayah_kabupaten SET wilayah_kabupaten.nama_prov = ";
    $sqlstr_upd = $sqlstr_upd . "'" . $data->nama . "'";
    $sqlstr_upd = $sqlstr_upd . " WHERE wilayah_kabupaten.kode_prov = ";
    $sqlstr_upd = $sqlstr_upd . "'" . $data->kode . "'";
    // echo $sqlstr_upd . PHP_EOL;

    $query_upd=$db->prepare($sqlstr_upd);
    $query_upd->execute();
    $query_upd = null;
}
$query=null;

?>
