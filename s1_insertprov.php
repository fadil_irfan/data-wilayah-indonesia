<?php 

$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '';
$dbname = 'wilayah';
$dbdsn = "mysql:dbname={$dbname};host={$dbhost}";
try {
  $db = new PDO($dbdsn, $dbuser, $dbpass);
} catch (PDOException $e) {
  echo 'Connection failed: '.$e->getMessage();
}

// INSERT kode_prov, nama_prov
$sqlstr = "INSERT INTO wilayah_provinsi(kode_prov, nama_prov) ";
$sqlstr = $sqlstr . "SELECT kode, nama ";
$sqlstr = $sqlstr . "FROM wilayah where char_length(kode)=2 ORDER BY kode";

$query = $db->prepare($sqlstr);
$query->execute();
$query = null;
$db = null;

?>
