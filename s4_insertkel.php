<?php 

$dbhost ='localhost';
$dbuser ='root';
$dbpass ='';
$dbname ='wilayah';
$dbdsn = "mysql:dbname={$dbname};host={$dbhost}";
try {
  $db = new PDO($dbdsn, $dbuser, $dbpass);
} catch (PDOException $e) {
  echo 'Connection failed: '.$e->getMessage();
}

// INSERT kode_prov, kode_kab, kode_kec, kode_kel, nama_kel
$sqlstr = "INSERT INTO wilayah_kelurahan(kode_prov, kode_kab, kode_kec, kode_kel, nama_kel) ";
$sqlstr = $sqlstr . "SELECT left(kode, 2), left(kode, 5), left(kode, 8), kode, nama ";
$sqlstr = $sqlstr . "FROM wilayah where char_length(kode)=13 ORDER BY kode";

$query = $db->prepare($sqlstr);
$query->execute();
$query = null;

// UPDATE nama_prov
echo "Updating nama_prov" . PHP_EOL;
$sqlstr = "SELECT kode, nama FROM wilayah WHERE CHAR_LENGTH(kode)=2 ORDER BY kode";
$query=$db->prepare($sqlstr);
$query->execute();

while ($data=$query->fetchObject()) {
    echo $data->kode.' '.$data->nama.PHP_EOL;
    $sqlstr_upd = "UPDATE wilayah_kelurahan SET wilayah_kelurahan.nama_prov = ";
    $sqlstr_upd = $sqlstr_upd . "'" . $data->nama . "'";
    $sqlstr_upd = $sqlstr_upd . " WHERE wilayah_kelurahan.kode_prov = ";
    $sqlstr_upd = $sqlstr_upd . "'" . $data->kode . "'";
    // echo $sqlstr_upd . PHP_EOL;

    $query_upd=$db->prepare($sqlstr_upd);
    $query_upd->execute();
    $query_upd = null;
}
$query=null;

// UPDATE nama_kab
echo "Updating nama_kab" . PHP_EOL;
$sqlstr = "SELECT kode, nama FROM wilayah WHERE CHAR_LENGTH(kode)=5 ORDER BY kode";
$query=$db->prepare($sqlstr);
$query->execute();

// while (false) {
while ($data=$query->fetchObject()) {
    // echo $data->kode.' '.$data->nama.PHP_EOL;
    $sqlstr_upd = "UPDATE wilayah_kelurahan SET wilayah_kelurahan.nama_kab = ";
    $sqlstr_upd = $sqlstr_upd . "'" . $data->nama . "'";
    $sqlstr_upd = $sqlstr_upd . " WHERE wilayah_kelurahan.kode_kab = ";
    $sqlstr_upd = $sqlstr_upd . "'" . $data->kode . "'";
    // echo $sqlstr_upd . PHP_EOL;

    $query_upd=$db->prepare($sqlstr_upd);
    $query_upd->execute();
    $query_upd = null;
}
$query=null;

// UPDATE nama_kec
echo "Updating nama_kec" . PHP_EOL;
$sqlstr = "SELECT kode, nama FROM wilayah WHERE CHAR_LENGTH(kode)=8 ORDER BY kode";
$query=$db->prepare($sqlstr);
$query->execute();

while ($data=$query->fetchObject()) {
    // echo $data->kode.' '.$data->nama.PHP_EOL;
    $nama_slash = addslashes($data->nama);
    // echo $nama_slash;
    $sqlstr_upd = "UPDATE wilayah_kelurahan SET wilayah_kelurahan.nama_kec = ";
    $sqlstr_upd = $sqlstr_upd . "'" . $nama_slash . "'";
    $sqlstr_upd = $sqlstr_upd . " WHERE wilayah_kelurahan.kode_kec = ";
    $sqlstr_upd = $sqlstr_upd . "'" . $data->kode . "'";
    echo $sqlstr_upd . PHP_EOL;

    $query_upd=$db->prepare($sqlstr_upd);
    $query_upd->execute();
    $query_upd = null;
}
$query=null;

?>
